#ifndef DLLEXPORTS_H
#defind DLLEXPORTS_H

#ifdef __dll__
#define IMPEXP __declspec(dllexport)
#else
#define IMPEXP __declspec(dllimport)
#endif 	// __dll__

#include "IEffectClass.h"


extern "C"
void* IMPEXP CreateIEffectClassInstance();


#endif	// DLLEXPORTS_H